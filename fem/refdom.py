from __future__ import annotations

import numpy as np


class Refdom:
    """Represent a finite element reference domain."""

    p: np.ndarray
    t: np.ndarray

    edges: np.ndarray
    facets: np.ndarray
    b_refdom: Refdom | None

    n_nodes: int = 0
    n_facets: int = 0
    n_edges: int = 0

    name: str = "Unknown"

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)


class RefdomPoint(Refdom):
    """Represent a reference point domain."""

    p = np.array([[0.0]], dtype=np.float64)
    t = np.array([[0]], dtype=np.int64)

    n_nodes = 1

    name = "Point"


class RefdomLine(Refdom):
    """Represent a reference line domain."""

    p = np.array([[0.0], [1.0]], dtype=np.float64)
    t = np.array([[0], [1]], dtype=np.int64)

    facets = np.array([[0], [1]], dtype=np.int64)
    edges = np.array([[0, 1]], dtype=np.int64)
    b_refdom = RefdomPoint()

    n_nodes = 2
    n_facets = 2

    name = "Line"


class RefdomTri(Refdom):
    """Represent a reference triangular domain."""

    p = np.array([[0.0, 0.0], [1.0, 0.0], [0.0, 1.0]], dtype=np.float64)
    t = np.array([[0], [1], [2]], dtype=np.int64)

    facets = np.array([[0, 1], [1, 2], [0, 2]], dtype=np.int64)
    edges = np.array([[0, 1], [1, 2], [0, 2]], dtype=np.int64)
    b_refdom = RefdomLine()

    n_nodes = 3
    n_facets = 3
    n_edges = 3

    name = "Triangle"


class RefdomTet(Refdom):
    """Represent a reference tetrahedral domain."""

    p = np.array(
        [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]],
        dtype=np.float64,
    )
    t = np.array([[0], [1], [2], [3]], dtype=np.int64)

    facets = np.array([[0, 1, 2], [0, 1, 3], [0, 2, 3], [1, 2, 3]], dtype=np.int64)
    edges = np.array([[0, 1], [0, 2], [0, 3], [1, 2], [1, 3], [2, 3]], dtype=np.int64)
    b_refdom = RefdomTri()

    n_nodes = 4
    n_facets = 4
    n_edges = 6

    name = "Tetrahedral"


class RefdomQuad(Refdom):
    """Represent a reference quadrangular domain."""


class RefdomHex(Refdom):
    """Represent a reference hex domain."""
