from abc import ABC, abstractstaticmethod, abstractmethod


class VectorSpace(ABC):
    @abstractstaticmethod
    def gbasis():
        pass


class H1(VectorSpace):
    @abstractmethod
    def gbasis():
        pass


class HDiv(VectorSpace):
    @abstractmethod
    def gbasis():
        pass


class HCul(VectorSpace):
    @abstractmethod
    def gbasis():
        pass
