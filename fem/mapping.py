from abc import ABC, abstractmethod


class Mapping(ABC):
    def F():
        """Return the forward mapping."""

    def det_F():
        """Return the determinant of the mapping."""

    def inv_F():
        """Return the inverse mapping."""
