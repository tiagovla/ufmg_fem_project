from fem.quadrature import QuadratureLine, QuadratureTet, QuadratureTri, get_quadrature
from fem.refdom import RefdomLine, RefdomTet, RefdomTri


class TestQuadratureLine:
    def test_init(self):
        QuadratureLine().get_quadrature(5)

    def test_factory_init(self):
        get_quadrature(RefdomLine(), 5)


class TestQuadratureTri:
    def test_init(self):
        QuadratureTri().get_quadrature(5)

    def test_factory_init(self):
        get_quadrature(RefdomTri(), 5)


class TestQuadratureTet:
    def test_init(self):
        QuadratureTet().get_quadrature(5)

    def test_factory_init(self):
        get_quadrature(RefdomTet(), 5)
